package restBackend.letsApp.dto;

import lombok.Data;

@Data
public class MetaDto {
    private String title;
    private String description;
    private String cover;

    public MetaDto(String title, String description, String cover) {
        this.title = title;
        this.description = description;
        this.cover = cover;
    }

}
