package restBackend.letsApp.dto;

public enum ObjectType {
    MESSAGE, COMMENT
}
