package restBackend.letsApp.dto;

public enum EventType {
    UPDATE, CREATE, REMOVE
}
