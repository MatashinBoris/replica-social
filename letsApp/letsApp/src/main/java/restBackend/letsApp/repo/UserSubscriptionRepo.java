package restBackend.letsApp.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import restBackend.letsApp.domain.User;
import restBackend.letsApp.domain.UserSubscription;
import restBackend.letsApp.domain.UserSubscriptionId;

import java.util.List;

public interface UserSubscriptionRepo extends JpaRepository<UserSubscription, UserSubscriptionId> {
    List<UserSubscription> findBySubscriber(User user);

    List<UserSubscription> findByChannel(User channel);

    UserSubscription findByChannelAndSubscriber(User channel, User subscriber);
}
