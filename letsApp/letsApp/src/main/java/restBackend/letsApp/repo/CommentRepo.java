package restBackend.letsApp.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import restBackend.letsApp.domain.Comment;

public interface CommentRepo extends JpaRepository<Comment, Long> {

}
