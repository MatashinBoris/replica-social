package restBackend.letsApp;

import io.sentry.Sentry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LetsAppApplication {

	public static void main(String[] args) {

		SpringApplication.run(LetsAppApplication.class, args);

	}

}
