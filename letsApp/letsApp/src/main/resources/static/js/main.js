import Vue from 'vue'
import App from "pages/App.vue";
import '@babel/polyfill'
import 'api/resource'
import router from 'router/router'
import {connect} from 'util/ws.js'
import store from 'store/store'
import Vuetify from "vuetify"
import 'vuetify/dist/vuetify.min.css'
import * as Sentry from "@sentry/browser"
import {Integrations} from "@sentry/tracing"

Sentry.init({
    Vue,
    dsn: "https://4388c462578b4597badecea84d207992@o507615.ingest.sentry.io/5598995",
    autoSessionTracking: true,
    integrations: [
        new Integrations.BrowserTracing(),
    ],

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
})

Sentry.configureScope(scope =>
    scope.setUser({
        id: profile && profile.id,
        username: profile && profile.name
    })
)

if (profile) {
    connect();
}

Vue.use(Vuetify);

new Vue({
    el: '#app',
    store: store,
    router,
    render: a => a(App),
    vuetify: new Vuetify()
})
